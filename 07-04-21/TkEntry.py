import tkinter as tk

main = tk.Tk()
main.geometry("200x300")
main.title("Tkinter Entry")

l1 = tk.Label(main, text="Enter Name")
en = tk.Entry(main, bg="pink", bd=5)

l2 = tk.Label(main, text="Enter Password")
pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="ridge")
# pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="flat")
# pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="groove")
# pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="sunken")
# pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="raised")


l1.pack()
en.pack()

l2.pack()
pwd.pack()

main.configure(bg='white')
main.mainloop()