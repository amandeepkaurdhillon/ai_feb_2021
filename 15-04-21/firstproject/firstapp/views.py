from django.shortcuts import render
from django.http import HttpResponse
from firstapp.models import Contact
import wikipedia
# Create your views here.

def index(request): 
    d={}
    if request.method=="POST":
        nm = request.POST["name"]
        em = request.POST["email"]
        msz = request.POST["message"]
    
        obj = Contact(full_name=nm,email=em, feedback=msz)
        obj.save()
        d["status"]="Data Saved Successfully!"
        # return HttpResponse("<h1>{} Thanks for your feedback!</h1>".format(nm))
    return render(request, "HTMLForm.html",d)
    # return HttpResponse("<h1>INDEX PAGE OF MY WEBSITE</h1>")

def about(request):
    return render(request,"about.html")

def wiki_api(request):
    context={}
    if request.method=="POST":
        sr = request.POST["item"]
        try:
            # wikipedia.set_lang("pun")
            # wikipedia.set_lang("hi")
            page = wikipedia.page(sr)
            print(page)
            context["title"]=page.title
            context["summary"]=page.summary 
            context["images"]=page.images
        except:
            context["summary"]=wikipedia.summary(sr)
            context["error"]="Could not find! Please try something else!"
    return render(request, "wiki.html", context)