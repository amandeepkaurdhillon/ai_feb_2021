#!/usr/bin/env python
# coding: utf-8

# ### 1D Array

# In[2]:


import numpy as np


# In[3]:


x = np.array([10,4,6,7,84,556,7])
print(x[2])


# In[6]:


print(x[1:4])
print(x[-3:])


# In[8]:


x[::-1]


# In[9]:


x[:4]


# In[10]:


x[::2]


# In[12]:


x[::-2]


# In[23]:


y = np.array((10,4,56,7,8))
i = y>=10


# In[25]:


print(i)
y[i]


# In[32]:


z = np.array([12,54,61,34,21,11])
print(z+2)
z[z%2]


# In[33]:


z[[0,0,1,1]]


# In[35]:


#Even values
z%2==0


# In[37]:


z[z%2==1]


# In[39]:


arr = np.array([343,5,6,223,88,235,77,80,64])
arr[arr%8==0]


# In[40]:


arr[arr%8!=0]


# ### 2D Array Indexing/Slicing/Step

# In[47]:


myar = np.array([
    [10,3,55],
    [12,45,66],
    [21,44,67],
    [3,76,8]
])
print("rows=",myar.shape[0])
print("cols=",myar.shape[1])


# In[52]:


myar[3,1]


# In[53]:


myar[1:]


# In[56]:


myar[1:2,1:]


# In[57]:


myar[1:3,:-1]


# In[61]:


a = np.array([
    [10,20,30],
    [40,50,60],
    [70,80,90],
])


# In[64]:


a[::-2, ::-2]


# In[68]:


a[1:,::-2]