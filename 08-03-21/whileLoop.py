# i = 1 #initialization
# while i<=10: #condition
#     print(i)
#     i+=1 #Increment


# i = 10
# while i>0:
#     print("Value of i is: {}".format(i))
#     i-=1

num = int(input("Enter Number: "))
i = 1
while i<=10:
    print("{} * {} = {}".format(num,i,num*i))
    i+=1

