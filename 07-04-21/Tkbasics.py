import tkinter as tk

#create Tk object (window)
win = tk.Tk()

#Resize Tkinter window
win.geometry("500x500")

#Add title to window
win.title("First Window")

#To customize sizing
win.resizable(False,False)


# To display window
win.mainloop()