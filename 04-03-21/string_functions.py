x = "AI stands for Artificial Intelligence"
y = "Python"

########################################
print(x)
print(x.lower())
print(x.upper())
print(x.title())
print(x.capitalize())
print(x.swapcase())
##########################################
print(y.isupper())
print(y.islower())
print(y.istitle())

x = "AI stands for Artificial Intelligence"

print(x.count("s"))
print(x.count("b"))
print(x.index("t"))
print(x.index("t",17))
print(x.find("b",5))
print(x.replace("s", "S"))
new = x.split("i")
print(new)
print(new[0])