import tkinter as tk
from tkinter import messagebox

main = tk.Tk()
main.geometry("200x300")
main.title("Tkinter Entry")

def abcd():
    # messagebox.showwarning("Warning!","Something Wrong is likely to happen!")
    # messagebox.showinfo("Greetings!","Hello There")
    messagebox.showerror("Failed!","OOPs, something went wrong!")

btn = tk.Button(main, text="click here", command=abcd)
btn.pack()

main.mainloop()