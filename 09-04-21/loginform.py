import tkinter as tk 
from tkinter import messagebox
import requests

main = tk.Tk()
main.geometry("300x400")
main.title("Register Form")
main.resizable(False, False)
ls = []
def abcd():
    username = un.get()
    email = em.get()
    pas = ps1.get()
    ls.append({"name":username, "email":email, "password":pas})
    try:
        with open("users.py","w") as f:
            f.write("all_users="+str(ls))
    except:
        messagebox.showerror("Error","Something went wrong!")
    else:
        messagebox.showinfo("Success","{} Registered Successfully!".format(username))

def login():
    from users import all_users

    l_email = login_email.get()
    l_pass = login_pass.get()
    found = False 
    for i in all_users:
        if i["email"]==l_email and i["password"]==l_pass:
            found=True  
            dashboard(i)
    if found==False:
        messagebox.showerror("Invalid Login","Sorry you are not a registered user")

def dashboard(user_details):
    loginwin.destroy()
    main.destroy()
    dashb = tk.Tk()
    dashb.title("Dashboard")
    dashb.config(bg="pink")
    dashb.geometry("300x400")

    t = "Mr./Ms. {}, Welcome To Our Site".format(user_details["name"])
    
    mylbl = tk.Label(dashb, text=t, font=("arial",20),bg="green",
    fg="white")
    mylbl.pack()
    global city_name
    city_label = tk.Label(dashb, text="Entyer City Name")
    city_label.pack()
    city_name = tk.Entry(dashb, relief="groove", bd=4)
    city_name.pack()
    weather_button = tk.Button(dashb, text="Get Weather", bg="green", fg="white", 
command=get_weather)
    weather_button.pack()

    global result
    result = tk.Label(dashb)
    result.pack(fill="x", padx=50, pady=20, ipady=10)
    
    mylbl.pack(ipady=50,fill="x")
    dashb.mainloop()

def get_weather():
    city = city_name.get()
    API = "http://api.openweathermap.org/data/2.5/weather?q={}&appid=c70a71da13017d87057e29848f4ff889&units=metric".format(city)
    data = requests.get(API).json()
    txt = ''
    txt += '{} ({}) \n'.format(data["name"], data["sys"]["country"])
    txt += 'Temperature: {}°C \n'.format(data["main"]["temp"])
    txt += '{} ({}) \n'.format(data["weather"][0]["main"], data["weather"][0]["description"])
    result.config(text=txt, bg="red", fg="white", font=("Arial", 20))


lb = tk.Label(main, text="Enter Username")
un = tk.Entry(main, relief="groove", bd=4)
eml = tk.Label(main, text="Enter Email")
em = tk.Entry(main, relief="groove", bd=4)
ps = tk.Label(main, text="Enter Password")
ps1 = tk.Entry(main, relief="groove", bd=4)
btn = tk.Button(main, text="Register", bg="green", fg="white", 
command=abcd)
# btn1 = tk.Button(main, text="Login", bg="orange", fg="white", 
# command=open_login)

lb.grid(row=0, column=0)
un.grid(row=0, column=1)
eml.grid(row=1, column=0)
em.grid(row=1, column=1)
ps.grid(row=2, column=0)
ps1.grid(row=2, column=1)
btn.grid(row=3, column=0)
# btn1.grid(row=3, column=1)

# LOGIN SCREEN
loginwin = tk.Tk()
loginwin.geometry("300x400")
loginwin.title("Loginn Form")
login_email_label = tk.Label(loginwin, text="Enter Email")
login_email = tk.Entry(loginwin, relief="groove", bd=4)
login_pass_label = tk.Label(loginwin, text="Enter Password")
login_pass = tk.Entry(loginwin, relief="groove", bd=4)
login_btn = tk.Button(loginwin, text="Login", bg="green", fg="white", 
command=login)
login_email_label.pack()
login_email.pack()
login_pass_label.pack()
login_pass.pack()
login_btn.pack()
main.mainloop()
