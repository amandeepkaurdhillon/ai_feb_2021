import tkinter as tk 

main = tk.Tk()
main.geometry("300x400")
main.title("Geometry Managers")
main.resizable(False, False)

lb = tk.Label(main, text="Enter Country Name")
en = tk.Entry(main, relief="groove", bd=4)
lb1 = tk.Label(main, text="Enter Country Name")
en1 = tk.Entry(main, relief="groove", bd=4)

# lb.pack()
# en.pack()

# lb1.place(x=50, y=50)
# en1.place(x=150, y=50)

lb.grid(row=0, column=0)
en.grid(row=0, column=1)

main.mainloop()