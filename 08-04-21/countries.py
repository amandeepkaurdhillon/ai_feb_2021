import tkinter as tk 
import requests

win = tk.Tk()
win.geometry("300x400")
win.title("Get Country Details")

def set_value():
    v = en.get()
    data = requests.get("https://restcountries.eu/rest/v2/all").json()
    rs = ""
    for i in data:
        if i["name"]==v:
            rs += "{} ({})\n".format(i["name"], i["capital"])
            rs += "Area: {}\n".format(i["area"])
            rs += "Population: {}\n".format(i["population"])
            rs += "Borders: {}\n".format(i["borders"])

    lb1.config(text=rs)

lb = tk.Label(win, text="Enter Country Name").pack()

en = tk.Entry(win, relief="groove", bd=4)
en.pack(fill="x",ipady=10)

btn = tk.Button(win, text="GET DETAILS", bg="green", fg="white",
command=set_value)
btn.pack(fill="x", pady=10)

lb1 = tk.Label(win, bg="yellow", fg="red", font=("arial",15))
lb1.pack(fill="x", pady=10)


win.mainloop()