print("============== Press '0' to Exit ==============")
while True:
    number = input("Enter Your Mobile Number: ")
    if number=="0":
        break
    if not number.isdigit():
        print("Number must contain digits only!")
    elif len(number)<10:
        print("Number must contain atleast 10 dgits!")
    else:
        con = len(number[:-3])*"*"+number[-3:]
        print("Your mobile number is: {}".format(con))