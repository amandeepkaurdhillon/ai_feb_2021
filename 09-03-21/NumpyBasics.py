#!/usr/bin/env python
# coding: utf-8

# ### Download Numpy

# In[1]:


get_ipython().system('pip install numpy')


# ### Import Numpy

# In[1]:


import numpy as np


# In[4]:


a = np.array([10,20,30])


# In[5]:


a.ndim


# In[6]:


a.dtype


# In[7]:


a.size


# In[14]:


b = np.array([[10,20],[30,40]], dtype=complex)
b.ndim
b


# ### Access Array Elements

# In[17]:


b[1][1]


# In[18]:


for i in b:
    print(i)


# ### Arrray Methods

# In[24]:


arr = np.array([10,4,6,87,4,77,8])
print("Max=",arr.max())
print("Min=",arr.min())
print(arr.mean())


# In[33]:


x = np.array([10,20,30,3,5,6])


# ### WAP to calculate mean of given array (x)

# In[34]:


s=0
c=0
for i in x:
    s= s+i
    c= c+1

print("Sum: ",s)
print("Count: ",c)
print("Mean: ",s/c)


# In[35]:


x.mean()


# In[37]:


np.median(x)