import tkinter as tk 
win = tk.Tk()
win.title("My Application")
def abcd():
    r = tk.Tk()
    r.title("New Window")
    r.mainloop()

menubar = tk.Menu(win)
home = tk.Menu(menubar, tearoff=0)
home.add_command(label="New", command=abcd)
home.add_command(label="New Window")
home.add_command(label="Open")
home.add_command(label="Save...")
home.add_separator()
home.add_command(label="Exit", command=win.destroy)
menubar.add_cascade(label="File", menu=home)

edit = tk.Menu(menubar, tearoff=0)
edit.add_command(label="Cut", command=abcd)
edit.add_command(label="Copy", command=abcd)
edit.add_command(label="Paste", command=abcd)
menubar.add_cascade(label="Edit",menu=edit)

win.config(menu=menubar)
win.mainloop()