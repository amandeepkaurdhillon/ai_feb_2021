import tkinter as tk
from tkinter import messagebox

main = tk.Tk()
main.geometry("200x300")
main.title("Tkinter Entry")

def get_data():
    un = en.get()
    pas = pwd.get()
    if un==pas:
        tk.messagebox.showinfo("Dashboard", "Welcome {}!!!".format(un))
    else:
        tk.messagebox.showerror("Error", "Invalid login details!"))

l1 = tk.Label(main, text="Enter Name")
en = tk.Entry(main, bg="pink", bd=5, relief="ridge")

l2 = tk.Label(main, text="Enter Password")
pwd = tk.Entry(main, bg="pink", bd=5,show="*", relief="ridge")

btn = tk.Button(main, text="Click To Continue", bg="green", fg="white",
font=("Cursive", 10), command=get_data)

l1.pack()
en.pack()
l2.pack()
pwd.pack()

btn.pack(pady=20)

main.configure(bg='white')
main.mainloop()