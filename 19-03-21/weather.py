import requests 
city = input("Enter City Name: ")
url = "http://api.openweathermap.org/data/2.5/weather?q={}&appid=e79a7d07c4e3bf54dd170836c3e88496&units=metric".format(city)
data = requests.get(url).json()
if data["cod"]==200:
    print("\n Here are weather details of {} ({})".format(data["name"],data["sys"]["country"]))
    print("\tTemperature: {}°C".format(data["main"]["temp"]))
    print("\tHumidity: {}".format(data["main"]["humidity"]))
    print("\t{} ({})".format(data["weather"][0]["main"],data["weather"][0]["description"]))
else:
    print("OOPs Something went wrong!")
    print(data["message"])