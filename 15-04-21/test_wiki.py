import wikipedia

# print(wikipedia.summary("America"))
print(wikipedia.search("America"))

# Change search result language
wikipedia.set_lang("hi")

pg = wikipedia.page("Kapil Sharma")
print(dir(pg))
print(pg.title)
print(pg.summary)
# print(pg.links)
print(pg.images)
