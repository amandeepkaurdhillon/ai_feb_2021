import tkinter as tk 
from tkinter import messagebox

main = tk.Tk()
main.geometry("300x400")
main.title("Login Form")
main.resizable(False, False)

def abcd():
    e = en.get()
    p = en1.get()
    if e==p:
       dashboard(e)
    else:
        messagebox.showerror("Error","Invalid Login details!")
    
lb = tk.Label(main, text="Enter Username")
en = tk.Entry(main, relief="groove", bd=4)
lb1 = tk.Label(main, text="Enter Password")
en1 = tk.Entry(main, relief="groove", bd=4)
btn = tk.Button(main, text="Register", bg="green", fg="white", 
command=abcd)

lb.grid(row=0, column=0)
en.grid(row=0, column=1)
lb1.grid(row=1, column=0)
en1.grid(row=1, column=1)
btn.grid(row=2, column=0,ipadx=80, columnspan=2)

def dashboard(name):
    loginwin = tk.Tk()
    loginwin.title("Dashboard")
    loginwin.config(bg="pink")
    t = "Mr./Ms. {}, Welcome To Our Site".format(name)
    mylbl = tk.Label(loginwin, text=t, font=("arial",20),bg="green",
    fg="white")
    mylbl.pack(ipady=50,fill="x")
    loginwin.mainloop()


main.mainloop()