import tkinter as tk

win = tk.Tk()
win.title("Tkinter Label")

# Create label widget
l1 = tk.Label(win, text="Hello Everyone!",fg="#612d0f", font=("Arial",20), 
bg="yellow")

l2 = tk.Label(win, text="How are you?",fg="#612d0f", font=("Arial",20), 
bg="yellow")

#Add label to tk window
l1.pack(ipadx=40, ipady=40, pady=40, fill="x", padx=50)
l2.pack()

win.geometry("300x500")
win.mainloop()