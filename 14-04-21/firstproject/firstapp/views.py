from django.shortcuts import render
from django.http import HttpResponse
from firstapp.models import Contact
# Create your views here.

def index(request): 
    d={}
    if request.method=="POST":
        nm = request.POST["name"]
        em = request.POST["email"]
        msz = request.POST["message"]
    
        obj = Contact(full_name=nm,email=em, feedback=msz)
        obj.save()
        d["status"]="Data Saved Successfully!"
        # return HttpResponse("<h1>{} Thanks for your feedback!</h1>".format(nm))
    return render(request, "HTMLForm.html",d)
    # return HttpResponse("<h1>INDEX PAGE OF MY WEBSITE</h1>")

def about(request):
    return render(request,"about.html")