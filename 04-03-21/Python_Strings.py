ab = "HELLO WORLD"
num = "0123456789"

print(num[::-3])
print(ab[::-1])

x = 2
# String Operations
print(ab+num) #Concatenation
print(x+4)
print("ab"*4)  #Iteration

xy = "Python is an easy Language"
## Membership Operation
print("is" in xy)
print("is" not in xy)
print("An" in xy)

if "Easy" in xy:
    print("String Found :)")
else:
    print("Not Found :(")