#pip install keras,tensorflow
from django.shortcuts import render
from tensorflow import keras
from keras.preprocessing.image import load_img,img_to_array
import matplotlib.pyplot as plt
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from django.http import HttpResponse, JsonResponse
from myapp.models import SearchResults
from django.core import serializers
from pathlib import Path
import os 
import wikipedia

BASE_DIR = Path(__file__).resolve().parent.parent
MEDIA_DIR = os.path.join(BASE_DIR, "media")


modl = keras.models.load_model('mymodel.h5')

# Create your views here.
def index(request):
    context = {}
    if request.method == "POST":
        if "img" in request.FILES:
            img = request.FILES.get("img")
            print("HELO", request.FILES)
            obj = SearchResults(image=img)
            obj.save()
            #serialize data to json
            d = serializers.serialize('json',[obj])
            context.update({"status":True,"message":"Image Uploaded Successfully!", "data":d})
            return JsonResponse(context)
        else:
            context.update({"status":False, "message":"Could Not Upload Image"})
            return JsonResponse(context)    
    return render(request,'index.html')


def predict_func(request):
    if request.method=="POST":
        id = request.POST.get("id")
        obj = SearchResults.objects.get(id=id)
    
        path = MEDIA_DIR+"/{}".format(obj.image)

        # Image Preprocessing and using VGG16 CNN Model (Deep Learning)
        img = load_img(path,target_size=(224,224))
        img1 = img_to_array(img)
        image = img1.reshape((1,224,224,3))
        image=preprocess_input(image)
        y = modl.predict(image)
        pred = decode_predictions(y,top=1000)
        p=pred[0][0][1].replace("_"," ")
        print("Prediction=",p)

        print(wikipedia.summary(p))
        print(wikipedia.page(p))
        return JsonResponse({"status":"success"})

    return JsonResponse({"status":"error"})